package ui.levitman.com.test;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.applicaster.util.ui.ap2dlist.AP2DListView;
import com.applicaster.util.ui.ap2dlist.AP2DListViewAdapter;

public class MainActivity extends AppCompatActivity {

    private AP2DListView twoDlistView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        twoDlistView = (AP2DListView) findViewById(R.id.lvSeasons);

        twoDlistView.setAdapter(new AP2DListViewAdapter(){

            @Override
            public int getNumOfRows() {
                return 50;
            }

            @Override
            public View getRow(final int row, View convertView, ViewGroup parent) {
                if(convertView == null){
                    LayoutInflater inflater = getLayoutInflater();
                    convertView = inflater.inflate(R.layout.row, parent, false);
                }

                return convertView;
            }

            @Override
            public Object getRowItem(int row) {
                return null;
            }

            @Override
            public int getNumOfItemsInRow(int row) {
                return 100;
            }

            @Override
            public View getCell(int row, int column, View convertView, final ViewGroup parent) {
                if(convertView == null) {
                    LayoutInflater inflater = getLayoutInflater();
                    convertView = inflater.inflate(R.layout.cell, parent, false);
                }
                return convertView;
            }

            @Override
            public Object getCellItem(int row, int column) {
                return null;
            }
        });
    }

}
